﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeaponControls : MonoBehaviour
{
	#region Public Weapon Related Stuff
	[SerializeField] private WeaponparticleEffectsPools weaponparticleEffectsPools;
	private Weapon currentWeapon;
	public Weapon CurrentWeapon { get => currentWeapon; }
	private List<Weapon> weapons = new List<Weapon>();
	public List<Weapon> Weapons { get => weapons; }
	private int currentWeaponIdx;
	public int CurrentWeaponIdx { get => currentWeaponIdx; }
	[SerializeField] private Transform weaponHolder;
	public Transform WeaponHolder { get => weaponHolder; }
	[SerializeField] private float minRangeForEffect;
	public float MinRangeForEffect { get => minRangeForEffect; }
	#endregion
	#region SerializeFields
	[SerializeField] private Transform weaponEmpty;
	[SerializeField] private float fOVAdd = 0.3f;
	[SerializeField] private float fovTimerFactor = 10f;
	[SerializeField] private float switchingFactor = 1f;
	[SerializeField] private float weaponEmptyRotation = 30f;
	#endregion
	#region Variables
	private PlayerManager playerManager;
	private PlayerUI playerUI;
	private bool isSwitching;
	private bool startedShooting;
	private bool lerpWeaponRot;
	private float fovTimer;
	private float switchingTimer = 0f;
	private float singleShootTimer;
	#endregion

	#region Player Standard Methods
	public void Init(PlayerManager _playermanager, PlayerUI _playerUI)
	{
		playerManager = _playermanager;
		playerUI = _playerUI;

		foreach (Transform child in weaponHolder)
		{
			weapons.Add(child.GetComponent<Weapon>());
		}
		for (int i = 0; i < weapons.Count; i++)
		{
			weapons[i].InitWeapon(playerManager, this, weaponparticleEffectsPools);
		}
		currentWeapon = weapons[currentWeaponIdx];

		Debug.Log("PlayerWeaponControls has succesfully been initialised");
	}
	public void OnUpdate()
	{
		singleShootTimer += Time.deltaTime;

		if (startedShooting)
		{
			if (fovTimer < currentWeapon.ChangeFOVTime)
			{
				fovTimer += fovTimerFactor * Time.deltaTime;
				playerManager.PlayerCam.fieldOfView = Mathf.Lerp(playerManager.PlayerCam.fieldOfView, playerManager.MaxFOV, fovTimer);
			}
			else if (playerManager.PlayerCam.fieldOfView < playerManager.DefaultFOV + fOVAdd)
			{
				fovTimer = 0f;
				playerManager.PlayerCam.fieldOfView = playerManager.DefaultFOV;
				startedShooting = false;
			}
			else
			{
				fovTimer += fovTimerFactor * Time.deltaTime;
				playerManager.PlayerCam.fieldOfView = Mathf.Lerp(playerManager.PlayerCam.fieldOfView, playerManager.DefaultFOV, fovTimer);
			}
		}

		if (lerpWeaponRot)
		{
			switchingTimer += Time.deltaTime;
			weaponEmpty.localRotation = Quaternion.Euler(weaponEmptyRotation - (weaponEmptyRotation * switchingTimer * switchingFactor), 0f, 0f);

			if (weaponEmpty.localRotation.x <= 0f)
			{
				Debug.Log("switching has stopped");
				weaponEmpty.localRotation = Quaternion.Euler(Vector3.zero);
				switchingTimer = 0f;
				lerpWeaponRot = false;
			}
		}
	}
	#endregion

	#region Public Methods
	public void Shoot()
	{
		if (isSwitching || currentWeapon.IsReloading) { return; }

		currentWeapon.ShootOneTime();
		startedShooting = true;
	}
	public void SingleShoot()
	{
		if (singleShootTimer > (1 / currentWeapon.FireRate))
		{
			singleShootTimer = 0f;
			currentWeapon.ShootOneTime();
			startedShooting = true;
		}
	}
	public void Reload()
	{
		currentWeapon.Reload();
	}
	public void SwitchWeapon()
	{
		if (isSwitching || currentWeapon.IsReloading) { return; }

		StartCoroutine("SwitchingWeapon");

		// deactivate old weapon
		currentWeapon.DeactivateThirdPersonWeaponInstance();
		currentWeapon.gameObject.SetActive(false);

		// reset weaponEmptyRotation
		weaponEmpty.localRotation = Quaternion.Euler(weaponEmptyRotation, 0f, 0f);

		int oldWeaponIdx = currentWeaponIdx;
		currentWeaponIdx++;

		if (currentWeaponIdx == weapons.Count) { currentWeaponIdx = 0; }

		currentWeapon = weapons[currentWeaponIdx];

		// activate new Weapon
		currentWeapon.ActivateThirdPersonWeaponInstance();
		currentWeapon.gameObject.SetActive(true);
		lerpWeaponRot = true;

		//playerUI.SwitchWeaponUI(oldWeaponIdx, currentWeaponIdx);
		Debug.Log("Weapon has been switched");
	}
	public void ChangeFireMode()
	{
		if (currentWeapon.CurrentFireMode == FireModes.SingleFire)
		{
			currentWeapon.CurrentFireMode = FireModes.FullAuto;
			Debug.Log($"FireMode changed to {currentWeapon.CurrentFireMode}");
		}
		else if (currentWeapon.CurrentFireMode == FireModes.FullAuto)
		{
			currentWeapon.CurrentFireMode = FireModes.SingleFire;
			Debug.Log($"FireMode changed to {currentWeapon.CurrentFireMode}");
		}
	}
	public void ResetWeapons()
	{
		for (int i = 0; i < weapons.Count; i++)
		{
			weapons[i].ReloadOnDeath();
		}
	}
	#endregion

	#region Coroutines
	IEnumerator SwitchingWeapon()
	{
		isSwitching = true;

		yield return new WaitForSeconds(playerManager.SwitchWeaponTime);

		isSwitching = false;
	}
	#endregion
}
